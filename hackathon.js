// Basic Setup
var http     = require('http'),
	express  = require('express'),
	mysql    = require('mysql');
	parser   = require('body-parser'),
	cors     = require('cors');
 
// Database Connection
var connection = mysql.createConnection({
  host     : '172.19.1.179',
  user     : 'root',
  password : '',
  database : 'server_master'
});

try {
	console.log("Creating Connection");
	connection.connect();
} catch(e) {
	console.log('Database Connetion failed:' + e);
}
 
// Setup express
var app = express();
app.use(cors());
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));
app.set('host', process.env.HOST || 2.16);
app.set('port', process.env.PORT || 7777);
 
// Set default route
app.get('/', function (req, res) {
	res.send('<html><body><p>Welcome to Hackthon App</p></body></html>');
});

// Create server
http.createServer(app).listen(app.get('port'), function(){
	console.log('Server listening on port ' + app.get('port'));
});