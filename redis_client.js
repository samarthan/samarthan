/*
 *Written By Akash Chandra
 */

var redis = require('redis');
var Promise = require("bluebird");
var async = require('async');

var config = require("./config");

function Redis_Client() {

    this.redisclient = redis.createClient(config.redis.redis_port, config.redis.redis_hostname, { password : "OibM5JGgMJhv4htk" });

    this.serviceCheck = function() {
        var redisserviceclient = this.redisclient;
        return new Promise(function(resolve, reject) {
            redisserviceclient.on("error", function(err) {
                if (err) {
                    console.log("Redis Client failed to Connect or Crashed  on IP : " + config.redis.redis_hostname + " PORT : " + config.redis.redis_port);
                    reject("Redis Failed to Connect on IP : " + config.redis.redis_hostname + " PORT : " + config.redis.redis_port);
                    //process.exit(1);
                }
            });
            redisserviceclient.on("connect", function() {
                console.log("Redis Client Connected Successfully");
                resolve('pass');
            });
        });
    };

    this.serviceQuitter = function() {
        if (this.redisclient) {
            console.log("Quitting Redis CLient ");
            this.redisclient.quit();
        }
    };

}

module.exports = Redis_Client;
