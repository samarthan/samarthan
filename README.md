Welcome to Samarthan Bot!
=======================


A bot that can do an initial tests for a product, do trouble shooting and provide a report of the steps it has taken to fix it. An <i class="icon-cog"></i> **Trouble Shooting** for microservices.

----------


Features
-------------

The Idea is to create a Self Learning Engineer to support the products of Comviva. In advent of microservices, and being from Ngage 7, we have around 130 microservices and many more to come. Managing such services from a engineer is pain.


> **Note:**

> - **Instant Health Checks** - Samarthan is capable of advance health checks in multiple containers through thousands of microservices 
> -  **Cognitive Framework** Self learning framework ensures priority based check and maintenance in error prone micro services and trouble shoot
> - **Plug n Play** - Samarthan can be easily plugged in with any complex product with thousands of microservices  
> - **Automated Troubleshooting** - Strong Samarthan can automated decisions for known issues and maintain high availability of business critical services 
> - **End-user experience monitoring** -  Bot can monitor and maintain user experience  24/7 through Chat APIs 
> - **Reports and Dashboard** - Samarthan is capable of generating reports and analytics on demand. 
> - **End to End transaction monitoring** - It is capable of tracing  a transaction from end to end  to max out the possible outbreak scenarios
> -  **Application topology visualization**  Samarthan is capable of  understanding the deployment topology and spread probes accordingly 
> - **Custom metric APIs Custom KPIs** and Metrics are key to identifying performance related issues 
> - **Custom logging APIs** Understanding log Jargons and proactive analysing patterns to track an transaction 


> **Tip:**  Refer [Heroku App](https://samarthan-bot.herokuapp.com/)
