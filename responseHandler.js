var redis = require("./redis_client");
var RedisCon = new redis();

var RedisClient = RedisCon.redisclient;
var Promise = require('bluebird');
var responseMap = require("./responseMap");
var DB = require('./ecp_db/index');
var sequelize = require('sequelize');
var request = require('request');

const STAGE_CONST = ":stage";
const TXN_COMMAND = ":command";

//Object Creation	
function ResponseHandler() {}
var method = ResponseHandler.prototype;

var jarvis = require("./executeCommands");
var Jarvis = new jarvis();




method.handleResponse = function(req, res) {
	var code = "";
	var promise = [];
	var obj = reqBodyExtractor(req);
	if(!obj){
		return res.json(reply(397));
	}
	var msg = "";

	checkStage(obj.userId,obj.channel).then(function(stage){
		if ( stage == -1 ) { 
			console.log("Base Response for UserID - > " + obj.userId);
			code = -1;
			//increment stage
			return insertResponseToDB(obj, stage).then(function (row) {
				return res.json(reply(code));
			});
			
		} else {
			//check the response string			
			return insertResponseToDB(obj, stage).then(function (row) {
				console.log("Saved Row is " + JSON.stringify(row));
				decodeResp(obj, stage, row).then(function (body) {
					if(body.code && body){
						code = body.code;
						console.log("Response from decodeResp " + body.msg);
						if (code == 201  || code == -810) { var json = reply(code,msg); json["pk"] = row.slNo; return res.json(json); }
						if (body.msg) msg = body.msg;						
						return res.json(reply(code,msg))
					}
					return res.json(reply(657))
				})
			}).catch(function(err){
				console.log("DB Insertion Failed" + err);
				return res.status(500).send(err);
			})
		}
	}).catch(function (err) {
		return res.json(reply(657));
	})
	
}

method.getPreviousExecStatus = function ( slNo ){
	return new Promise(function(resolve,reject) {
		DB.response_master.findOne({where : { "slNo" : slNo }}).then(function(out) {
			if(out && out.output != null){
				return resolve({status : out.execStatus, output : out.output});	
			}
			return resolve({ status: "-2", msg : "Command Execution is still in progress.. "})
		}).catch(function(e) {
				console.log("error at 282"+ e)
			return reject(false);
		})
	});
}

method.checkCommandRunningStatus = function (userid,channel){
	var channel = channel ? channel : "fb";
	return new Promise(function(resolve,reject){
		var self = this;
		RedisClient.get(userid + ":" + channel + TXN_COMMAND, function(err, reply) {
			console.log("hash Check for user ID " + userid + " is -> " + reply);
			if(err) { console.log("hash Check for user ID " + userid + " has failed -> " + err); reject(err); }
		    // reply is null when the key is missing
		    if(reply){
		    	resolve(reply);
		    } else {
		    	//no such command in redis 
		    	console.log("Couldn't find any history of the User id -> " + userid)
		    	resolve("-2");
		    }
		});
	})
}

method.checker = function (req,res) {
	RedisClient.hgetall("abc",function(err, reply) {
		if(err) res.send(err);
	    // reply is null when the key is missing
	    res.send(reply);
	});
}

//////////////////////////////////////////////////////



///////////////////////////////////////////////////////

function reqBodyExtractor(req){
	var body = {};
	if(req.body.userId && req.body.channel && req.body.message){
		body.userId = req.body.userId;
		body.channel = req.body.channel;
		body.message = req.body.message;
		body.timestamp = new Date();
		console.log("Decoded the body as -> " + JSON.stringify(body));
		return body;
	} else {
		return false;
	}
}


function decodeResp(obj, stage, row) {
	var response = obj.message.toLowerCase();
	//return Promise.resolve(reply(-400));


	if(!obj.message){
		console.log("Message not found in object!");
		return Promise.resolve(reply(-400));
	}

	// 
	else if(response.includes("help")){
		return helpExecutor();
	}	


	else if(response.includes("system")){
		return systemExecutor();
	}

	else if(response.includes("yes")){
		return yesExecutor(obj, stage, row);
	}

	else if(response.includes("check")){
		//check if previos command is still executing
		console.log("Message has execution Orders");
		return checkCommandRunningStatus(obj.userId,obj.channel).then(function(redis_reply){
			if(redis_reply == "-1"){
				return Promise.resolve(reply(-909));
			} else {
				return allCommandsExecutor(obj, stage, row);
			}	
		}).catch(function(err){
			return Promise.reject(err);
		})
    }
    
	// 	//check if the level exists in DB
    else if(response.includes("trouble")){
		return yesExecutor(obj, stage, row);

	}
    
    else if(response.includes("exit")  ||  response.includes("bye") ){
    	return delCommandRunningStatus(obj.userId,obj.channel).then(function(redis_reply){
    		return Promise.resolve(reply(6));
    	});
   	}

	else{
		console.log("Couldn't match with any of our commands");
		return Promise.resolve(reply(-400));
	}
}




///////////////////////////////////////////////
///////////Internal Helper Functions //////////
///////////////////////////////////////////////


function helpExecutor(obj, stage){
    return new Promise(function(resolve,reject){

        //fetch all levels from db - and provide with proper message
        DB.troubleshoot_master.findAll({
            attributes: [
                [sequelize.fn('DISTINCT', sequelize.col('level')) ,'level'],
            ]
        }).then(function(stageName) {  
            console.log("level "+JSON.stringify(stageName));
            var levelArr = [];
            var i = 1;
            stageName.map( function(levels) {
                levelArr.push("\n"+i+". Check "+levels.level);
                i++;
            });
            levelArr.push("\nGimme the level you want to execute \n\nWanna troubleshoot ? Gimme reply as \"troubleshoot\"\n\nSay \"bye\" or \"exit\" to exit\n");
            
            var resp = {
                code : "200",
                msg : "Yo! We have "+ stageName.length + " levels of check \n"
                        + levelArr
            }
            console.log(resp.msg)
             resolve(resp);
            }).catch(function(e) {
             reject(e);
         });
    })  
}


function systemExecutor(obj, stage) {
    return new Promise(function(resolve, reject) {
        //fetch all vms from db - and provide with proper message
        DB.server_master.findAll({
            attributes: ['vmIp', 'vmCategory']
        }).then(function(resp) {
            
            var levelArr = [];
            var i = 1;
            resp.map(function(vm) {
                levelArr.push(i + ". " + vm.vmIp + "  " + vm.vmCategory);
                i++;
            });
            console.log(levelArr);
            var response = {
                code: "200",
                msg: "Below are the VM IPs and their category \n"+levelArr.join("\n")
            }
            console.log("***********", JSON.stringify(response));
            resolve(response);
        }).catch(function(e) {
            reject(e);
        })
    })
}





function yesExecutor(obj, stage, row) {
    return checkCommandRunningStatus(obj.userId,obj.channel).then(function(res){
    	if(res == -1){
    		return Promise.resolve(reply(-910));
    	}else{
    		return fetchThetroubleCommandsFromDB(stage).then(function(body) {
    			if ( body.code == "-808") {
    				return Promise.resolve(body);
    			} else {
    				console.log("Respone Handled")
					Jarvis.troubleshootExecution(body.cmd, obj, row);
					return Promise.resolve(reply("-810"));
    			}
				
			});
    	}
    })
	
}


function allCommandsExecutor(obj, stage, row){ 
	console.log("inside all commands executors ",obj,stage,row);
	return fetchTheCommandsFromDB(obj.message).then(function(body) {
		if ( body.code == "-808") {
			return Promise.resolve(body);
		} else{
		    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		    console.dir(body);
			console.log("Respone Handled")
			Jarvis.commandExecution(body.cmd, obj, row).then(function(lol){
				console.log("Command Executed successfully");
			});
			return Promise.resolve(reply(201));
			return updateRedisStatus(obj, body.stage).then( function(resp) {
				return Promise.resolve(reply(201));
			})
		}
	});
}

//////////////////////////////////////////////////////////////


function fetchThetroubleCommandsFromDB(stage) {
    var cmd = [];

    return fetchStageName(stage).then(function(levelOrder){
    	return DB.troubleshoot_master.findAll({
	        where: sequelize.where(sequelize.fn('lower', sequelize.col('level')),
	            sequelize.fn('lower', levelOrder))
	    }).then(function(rows) {
	    	console.log("{{{{{{{{{{{{}}}}}}}}}}}}}}}}"+rows.length);
	    	if(rows){
	    		var promises = rows.map(function(row){
	    			row = row.toJSON();
	    			console.log("{{{{{{{{{{{{}}}}}}}}}}}}}}}}"+row);
	    			var stage = row.levelOrder;
    				return DB.server_master.findAll({where : { vmCategory : row.vmCategory }
						}).then(function(server_details) {
							for(var srv in server_details){
								var server = server_details[srv];
								row.server = server.toJSON();
							}
							return row;
						});
	    		})
	        	return Promise.all(promises).then(function(real_commands) {
	        		return ({cmd : real_commands, stage : stage});	
	        	});
	    	} else {
	    		return Promise.resolve(reply(-808));
	    	}
	    })
    })
}


function fetchTheCommandsFromDB(message) {
    var message = message.toLowerCase().split("check").join("").trim();
    console.log("Inside fetchthecommandsfromdb");
    console.log("Message sent by user "+message);
    if (message) {
        var cmd = [];
        var stage = "";
        return DB.troubleshoot_master.findAll({
            where: sequelize.where(sequelize.fn('lower', sequelize.col('level')),
                sequelize.fn('lower', message))
        }).then(function(rows) {
        	console.log("Rows fetched for the message "+rows);
        	if(rows && rows.length >0){
        		var promises = rows.map(function(row){
        			row = row.toJSON();
        			stage = row.levelOrder;
        				return DB.server_master.findAll({where : { vmCategory : row.vmCategory }
							}).then(function(server_details) {
								for(var srv in server_details){
									var server = server_details[srv];
									row.server = server.toJSON();
								}
								console.log("row after server attachment "+ row);
								return Promise.resolve(row);
							});
        		})
            	return Promise.all(promises).then(function(real_commands){
            		return ({cmd : real_commands, stage : stage});
            	});	
        	} else {
        		return Promise.resolve(reply(-808));
        	}
        })
    } else {
        return Promise.resolve(reply(-808));
    }
}


function fetchStageName (stage) {
	return new Promise(function(resolve,reject){
        DB.troubleshoot_master.findOne({where : { levelOrder : stage }
    	}).then(function(stageName) {
    	  console.log("Fetch of stage name is successful level" + stageName.level);
          return resolve(stageName.level);
       	}).catch(function(e) {
          return reject(e);
       	});
    });
}

//////////////////////////////////////////////////////////////


function fetchPreviousCommandOutput(obj) {
	return new Promise(function(resolve,reject){
		console.log("txn "+obj.userId + ":" + obj.channel + TXN_COMMAND);
		RedisClient.get(obj.userId + ":" + obj.channel + TXN_COMMAND, function(err, reply) {
			console.log("hash Check for user ID " + obj.userId + " is -> " + reply);
			if(err) { console.log("hash Check for user ID " + obj.userId + " has failed -> " + err); reject(err); }
		    // reply is null when the key is missing
		    if(reply){
		    	resolve(reply);
		    } else {		    	
		    	resolve("1");
		    }    
		});
	})
}

function reply(code,msg){
	console.log("Extractor : " + msg);
	var message = msg ? msg : responseMap[code];
	console.log("Forming the message with " + code + " :: " +  message );
	if( code == 201) {
		return { code : code , msg : message, flag : true };
	}
	return { code : code , msg : message };
}

function insertResponseToDB(req, state) {
	console.log("insertResponseToDB 270"+req)
	var dbData = {
		state : state,
		userId : req.userId,
		response : req.message,
		timeStamp : new Date()
	}

	return new Promise(function(resolve,reject){
	   console.log("Inserting response to DB failed ");
       DB.response_master.create(dbData).then(function(out) {
       	  console.log("Insert response to DB successfull");
          return resolve(out.toJSON());
       }).catch(function(e) {
       	  console.log("Failed to insert response to DB" + e);
          return reject(false);
       });
    });
}

function updateRedisStatus(obj, stage) {
	
	return new Promise( function(resolve, reject) {
		RedisClient.set(obj.userId + ":" + obj.channel + STAGE_CONST, stage, function(err, reply) {
			console.log("hash set for user ID " + obj.userId + " is -> " + reply);
			if(err) { console.log("hash set for user ID " + obj.userId + " has failed -> " + err); reject(err); }
		    // reply is null when the key is missing
		    if(reply){
		    	resolve(true);
		    }
		});
	})
	
}

function checkStage(userid,channel){
	var channel = channel ? channel : "fb";
	return new Promise(function(resolve,reject){
		var self = this;
		RedisClient.get(userid + ":" + channel + STAGE_CONST, function(err, reply) {
			console.log("hash Check for user ID " + userid + " is -> " + reply);
			if(err) { console.log("hash Check for user ID " + userid + " has failed -> " + err); reject(err); }
		    // reply is null when the key is missing
		    if(reply){
		    	resolve(reply);
		    }
		    else {
		    	RedisClient.set(userid + ":" + channel + STAGE_CONST , 1, function(err, reply) {
					console.log("hash Check for user ID " + userid + " is -> " + reply);
					if(err) { console.log("hash Check for user ID " + userid + " has failed -> " + err); reject(err); }
				    // reply is null when the key is missing
				    	resolve("-1");
				});
			}
		});
	});
}

function checkCommandRunningStatus(userid,channel){
	var channel = channel ? channel : "fb";
	return new Promise(function(resolve,reject){
		var self = this;
		RedisClient.get(userid + ":" + channel + TXN_COMMAND, function(err, reply) {
			console.log("COMMAND Check for user ID " + userid + " is -> " + reply);
			if(err) { console.log("COMMAND Check for user ID " + userid + " has failed -> " + err); reject(err); }
		    // reply is null when the key is missing
		    if(reply){
		    	resolve(reply);
		    } else {
		    	//no such command in redis 
		    	console.log("Couldn't find any history of the User id -> " + userid)
		    	resolve("-2");
		    }
		});
	})
}

function delCommandRunningStatus(userid,channel){
	var channel = channel ? channel : "fb";
	return new Promise(function(resolve,reject){
		var self = this;
		RedisClient.del(userid + ":" + channel + "*", function(err, reply) {
			console.log("Made the Command status as not running " + userid + " is -> " + reply);
			if ( err) { console.log("Made the Command status as not running " + userid + " has failed -> " + err); reject(err); }
			// reply is null when the key is missing
			if(reply >= 0 ){
				resolve("1");
			} else {
				//no such command in redis 
				console.log("Couldn't find any history of the User id -> " + userid)
				resolve("-2");
			}
		});
	})
}



module.exports = ResponseHandler;
