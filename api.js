"use strict;"

var Promise = require('bluebird');
var models = global.DB;

function API() {}

method = API.prototype;


method.getDet = function(req, res) {
    models.server_master.findAll({
        attributes: ['vmCategory']
    }).then(function(resp) {
        if (resp && resp.length > 0) {
            console.log("Fetching the details successfully");
            res.json(resp);
        } else {
            console.log("Attribute not found");
            res.status(500).json({
                msg: "no records"
            });
        }
    }).catch(function(err) {
        console.log("Error while getting details:", err);
        res.status(400).json({
            msg: err
        });
    });
};


method.getDetails = function(req, res) {
    models.server_master.findAll({
        where: req.body

    }).then(function(rows) {
        if (rows && rows.length > 0) {
            console.log("Listing all details successfully");
            res.json(rows);
        } else {
            console.log(" list not found");
            res.status(500).json({
                status: "errors",
                description: "No records"
            });
        }
    }).catch(function(err) {
        console.log("Error while getting details:", err);
        res.status(400).json({
            msg: err
        });
    });
};



method.saveDetail = function(req, res) {
    var data = JSON.parse(JSON.stringify(req.body));
    console.log(data); //   console.log(obj.parse(req.body));
    // for (var i = 0; i < jsonData.length; i++) {
    // var counter = jsonData[i];
    //  console.log(counter);
    models.server_master.create(data)
        .then(function(resp) {

            console.log("Created a new entries successfully");
            res.status(201).json({
                msg: "resource created"
            });
        }).catch(function(err) {
            console.log("Error while creation of new entry", err);
            res.status(500).json({
                msg: "Internal server error"
            });
        });
};


method.deleteDetail = function(req, res) {
    models.server_master.findOne({
        where: { slNo: req.params.id }
    }).then(function(response) {
        if (response) {
            models.server_master.destroy({
                where: { slNo: req.params.id },
                force: true
            }).then(function(resp) {
                if (resp) {
                    console.log("Deleted successfully");
                    res.json("Deleted Successfully");
                }
            });
        } else {
            console.log("entry not found");
            res.status(404).json({
                message: "entry not found"
            });
        }
    }).catch(function(err) {
        console.log("Error while deleting the entry", err);
        res.status(400).json({
            msg: err
        });
    });
};

method.getList = function(req, res) {
    models.troubleshoot_master.findAll({
        where: req.body //,

    }).then(function(rows) {
        if (rows && rows.length > 0) {
            console.log("Listing all details successfully");
            res.json(rows);
        } else {
            console.log(" list not found");
            res.status(500).json({
                status: "errors",
                description: "No records"
            });
        }
    }).catch(function(err) {
        console.log("Error while getting details:", err);
        res.status(400).json({
            msg: err
        });
    });
};


method.saveList = function(req, res) {
    console.log(req.body);
    models.troubleshoot_master.create(req.body)
        .then(function(resp) {

            console.log("Created a new entry successfully");
            res.status(201).json({
                msg: "resource created"
            });
        }).catch(function(err) {
            console.log("Error while creation of new entry", err);
            res.status(500).json({
                msg: "Internal server error"
            });
        });
};


method.deleteList = function(req, res) {
    models.troubleshoot_master.findOne({
        where: { slNo: req.params.id }
    }).then(function(response) {
        if (response) {
            models.troubleshoot_master.destroy({
                where: { slNo: req.params.id },
                force: true
            }).then(function(resp) {
                if (resp) {
                    console.log("Deleted successfully");
                    res.json("Deleted Successfully");
                }
            });
        } else {
            console.log("entry not found");
            res.status(404).json({
                message: "entry not found"
            });
        }
    }).catch(function(err) {
        console.log("Error while deleting the list", err);
        res.status(400).json({
            msg: err
        });
    });
};



module.exports = API;