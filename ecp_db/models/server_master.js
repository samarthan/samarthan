"use strict;"

module.exports = function (sequelize, Types) {
    var ServerMaster = sequelize.define("server_master", {
        slNo:        { type: Types.INTEGER(100), primaryKey: true,autoIncrement:true, allowNull: false },
        vmIp:        { type: Types.STRING(100), allowNull: false },
        userName:    { type: Types.STRING(250), allowNull: false },
        loginType:   { type: Types.STRING(50), allowNull: false },
        keyorPass:   { type: Types.STRING(66), allowNull: false },
        vmCategory:  { type: Types.STRING(500), allowNull: false },
        sshPort:     { type: Types.INTEGER(100), allowNull: false },
    },
        {
            paranoid: true,
            table_name: "server_master",
            timestamps: true,
        });
    return ServerMaster;

};
