"use strict;"

module.exports = function (sequelize, Types) {
    var ResponseMaster = sequelize.define("response_master", {
        slNo: { type: Types.INTEGER(100), primaryKey: true, autoIncrement: true, allowNull: false },
        userId: { type: Types.INTEGER(11), allowNull: false },
        state: { type: Types.INTEGER(11), allowNull: false }, 
        response: { type: Types.STRING(100), allowNull: false },
        timeStamp: { type: Types.DATE(), allowNull: false },
        output: { type: Types.STRING(10000), allowNull: true },
        execStatus: { type: Types.INTEGER(5), allowNull: true },
        command: { type: Types.STRING(2000), allowNull: true },

    },
        {
            paranoid: true,
            table_name: "response_master",
            timestamps: true,
        });
    return ResponseMaster;

};
