"use strict;"

module.exports = function (sequelize, Types) {
    var TroubleshootMaster = sequelize.define("troubleshoot_master", {
        slNo: { type: Types.INTEGER(100), primaryKey: true,autoIncrement:true, allowNull: false },
        vmCategory: { type: Types.STRING(5000), allowNull: false },    
        commands: { type: Types.STRING(20000), allowNull: false },
        expectedOutput: { type: Types.STRING(4000), allowNull: false },
        troubleshootCommands: { type: Types.STRING(5000), allowNull: false },
        outputType: { type: Types.BOOLEAN(), allowNull: false },
        SSHPort: { type: Types.INTEGER(100), allowNull: false },
        levelOrder: { type: Types.INTEGER(11), allowNull: false },
        level: { type: Types.STRING(100), allowNull: false },
        parentId: { type: Types.INTEGER(100), allowNull: false },
        
    },
        {
            paranoid: true,
            table_name: "troubleshoot_master",
            timestamps: true,
        });
    return TroubleshootMaster;

};
