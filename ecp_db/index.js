
'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var dbDir     = path.join(__dirname, "models");
//var xlsx      = require('node-xlsx');
var _         = require('underscore');
var db        = {};
//var async     = require('async');
var md5       = require('blueimp-md5');
var Promise   = require('bluebird');
var config    = require ('../config.json');
global.config  = config;


if (!global.config) throw new Error("Global var for DB is not set. Exiting...");
var config = {
  host: global.config.host,
  dialect: global.config.dialect,
  pool: global.config.pool
}
var sequelize = new Sequelize(global.config.database, global.config.username, global.config.password, config);

fs
  .readdirSync(dbDir)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename);
  })
  .forEach(function(file) {
    if (file.slice(-3) !== '.js') return;
    var model = sequelize['import'](path.join(dbDir, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
