///////////////////////////////////////////////////////////////
//////////////////////BOT BASED CHANGES ///////////////////////
///////////////////////////////////////////////////////////////


var Promise = require("bluebird");
var request = require("request");
const BootBot = require('bootbot');
var TelegramBot = require('node-telegram-bot-api');
var restify = require('restify');
var builder = require('botbuilder');


// Be sure to replace YOUR_BOT_TOKEN with your actual bot token on this line.
telegram = new TelegramBot("503748972:AAHW0uXjp7gCXDeqEkM-BsU71PAk--YH4Fc", { polling: true });



telegram.on("text", (message) => {
    poster(message.chat.id, "telegram", message.text).then(function(resp){
        telegram.sendMessage(message.chat.id, resp.msg);
        if ((resp.code == 201 || resp.code == -810) && resp.pk){
            console.log("Starting the recaller process for Telegram");
            ensureCheckIsTrue(resp.pk, 2000).then(function (response) {
                //response = JSON.parse(response);
                var STATUS; var extra = "";
                if (response.status == 0) { STATUS = "FAILURE";  extra = "It seems the Health Check has failed, we can trouble shoot for you, write TROUBLESHOOT to us" } else { STATUS = "SUCCESS"} 
                telegram.sendMessage(message.chat.id, "Execution seems to be completed just now with output  \n " + "STATUS : " + STATUS + "\n OUTPUT : " + response.output + "\n " + extra);
            });
        }
    })
});



const fb_bot = new BootBot({
    accessToken: 'EAAZAGmRv3ucgBANLcwZB9krwu6naLWJI9ZAla1iz2ZAiPk7BFtXZCplAgD8xaicpNVtMQ9wYxTPBBeGBJZCwFavEjFPsrJ01ZC4ZBtAChCl1O6oZBOB947GMrZCIhctIbDVOZCt0aFjDoMhu0XdZCGD02ZBmQelcTp3zjRzle4nWc90KNbgZDZD',
    verifyToken: 'samarthanBOT',
    appSecret: 'd7a23840bd6301fb1e32067aa683024c'
});

fb_bot.on('message', (payload, chat) => {
    poster(chat.userId, "facebook", payload.message.text).then(function (resp) {
        chat.say(resp.msg);
        if ((resp.code == 201 || resp.code == -810) && resp.pk){
            console.log("Starting the recaller process for Facebook");
            ensureCheckIsTrue(resp.pk, 2000).then(function (response) {
                var STATUS, extra = "";
                if (response.status == 0) { STATUS = "FAILURE"; extra = "It seems the Health Check failed, we can trouble shoot for you, write TROUBLESHOOT to us" } else { STATUS = "SUCCESS" } 
                chat.say("Execution seems to be completed just now with output  \n " + "STATUS : " + STATUS + "\n OUTPUT : " + response.output + "\n " + extra);
            });
        }
    })
});


fb_bot.start();




//=========================================================
// Bot Setup
//=========================================================
// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 4000, function () {
    console.log('%s listening to %s', server.name, server.url);
});
// Create chat bot
var connector = new builder.ChatConnector({
    appId: "55167b33-7e93-492b-9018-2880243dc2c9",
    appPassword: "oxdPBJ451)gvqwEBML34!@["
});
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());
//Bot on
bot.on('contactRelationUpdate', function (message) {
    if (message.action === 'add') {
        var name = message.user ? message.user.name : null;
        var reply = new builder.Message()
            .address(message.address)
            .text("Hello %s... Thanks for adding me. Say 'hello' to see some great demos.", name || 'there');
        bot.send(reply);
    } else {
        // delete their data
    }
});
bot.on('typing', function (message) {
    // User is typing
});
bot.on('deleteUserData', function (message) {
    // User asked to delete their data
});
//=========================================================
// Bots Dialogs
//=========================================================
String.prototype.contains = function (content) {
    return this.indexOf(content) !== -1;
}
bot.dialog('/', function (session) {
    poster(session.message.user.id, "skype", session.message.text).then(function (resp) {
        session.send(resp.msg);
    })
});



function poster(username, channel, message){
    return new Promise(function(resolve,reject){
        var options = {
            method: 'POST',
            url: 'http://http://fc1f5cfd.ngrok.io:80/submit',
            headers:
                {
                    'content-type': 'application/json'
                },
            body: { userId: username, channel: channel, message: message },
            json: true
        };
        request(options, function (error, response, body) {
            if (error) { 
                console.log("Error Happened while posting " + error);
                resolve({msg : "Uyii, Our Servers just went down because of Hurricane, Try again maadi"})
            }
            resolve(body);
        });
    })
}

function recaller(obj, channel, interval, id, slNo){
    ensureCheckIsTrue(slNo, interval).then(function(response){
        if(channel == "telegram"){
            console.log(id);
            return obj.sendMessage(id, "Execution seems to be completed just now with output  : " + JSON.stringify(response));
        } else if (channel == "facebook"){
            return obj.say("Execution seems to be completed just now with output  : " + JSON.stringify(response));
        }
    })
}

function ensureCheckIsTrue(slNo, interval) {
    return new Promise(function (resolve, reject) {
        (function waitForCheck() {

            var options = {
                method: 'GET',
                url: 'http://172.19.4.80:7777/getExecStatus/' + slNo,
            };

            request(options, function (error, response, body) {
                body = JSON.parse(body);
                if (body && body.status != "-2") {
                    console.log("Command Execution successful in background");
                    return resolve(body);
                }
                console.log("Sleeping now for " +interval+ " ms.");
                setTimeout(waitForCheck, interval);
            });
        })();
    });
}



//////////////////////////////////API Changes////////////////////////////////////////////////
//Commented as not needed in Heroku

/*
var config = require("./config");
global.config = config;
var express = require ('express');
var responseHandler = require("./responseHandler");
var ResponseHandler = new responseHandler();
var DB = require('./ecp_db/index');
global.DB = DB;


var http     = require('http'),
    express  = require('express'),
    mysql    = require('mysql');
    parser   = require('body-parser'),
    cors     = require('cors');

 
// Setup express
var app = express();
app.use(cors());
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));
app.set('port', process.env.PORT || 7777);

var api = require("./api.js");
var api = new api();

app.get('/', function (req, res) {
  res.send('<html><body><p>Welcome to Hackthon App</p></body></html>');
});

app.get("/serverMaster/vmips",api.getDet);
app.get("/serverMaster",api.getDetails);
app.post("/serverMaster",api.saveDetail);
app.delete("/serverMaster/:id",api.deleteDetail);

app.get("/troubleshootMaster",api.getList);
app.post("/troubleshootMaster",api.saveList );
app.delete("/troubleshootMaster/:id",api.deleteList);

app.post('/submit', function(req,res) {
    ResponseHandler.handleResponse(req,res);
});

app.get('/getExecStatus/:slNo', function( req, res) {
    var slNo = req.params.slNo;
    ResponseHandler.getPreviousExecStatus(slNo).then(function(out) {
        res.send(out);
    });
});


app.get('/isCommandRunning/:userId/:channel', function( req, res) {
    var obj = {
        userId : req.params.userId,
        channel : req.params.channel  
    };
    ResponseHandler.checkCommandRunningStatus(obj.userId, obj.channel).then(function(resp){
        console.log("Checking commad status for  userid ", obj.userId ,resp);
        res.json({status : resp});
    });
});

DB.sequelize.sync().then(function(msg) {
    http.createServer(app).listen(app.get('port'), function(){
        console.log('Server listening on port ' + app.get('port'));
    });
});


*/
