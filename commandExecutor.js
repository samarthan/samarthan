var node_ssh = require('node-ssh');
var redis = require("./RedisClient");

var DB = require('./ecp_db/index');

var RedisCon = new redis();
var RedisClient = RedisCon.redisclient;

const arrayVM = [
	{ "host" : '172.19.1.179', "port" : '22', "username" : 'root', "password" : 'qwerty'},
	{ "host" : '172.19.1.179', "port" : '22', "username" : 'root', "password" : 'qwerty'},
	{ "host" : '172.19.1.179', "port" : '22', "username" : 'root', "password" : 'qwerty'},
	{ "host" : '172.19.1.179', "port" : '22', "username" : 'root', "password" : 'qwerty'},
]

/*function getVMArray(){
	// var vm_array = [];
	return DB.server_master.findAll({
		where: {
			vmCategory: "lalle"
		}
	})
	// .then((rows) => {
	// 	// console.log("Data: ", rows);
	// 	rows.map(function(ele){
	// 		vm_array.push(ele.dataValues)
	// 	})
	// 	return vm_array;
	// })
}*/

function commandExecutor(req){
	/* vm_array = [];
	getVMArray()
	.then(function(rows){
		rows.map(function(ele){
			vm_array.push(ele.dataValues)
		})
	}); 
	console.log("vm_array: ", vm_array)*/
	var promises = arrayVM.map(function(server){
		var host = server.host;
		var port = server.port ? server.port : 22;
		var username = server.username;
		var password = server.password;
		return executeCommand(host, port, username, password, "whoami")
	})	
	return Promise.all(promises)
	.then(function(res){
		// console.log("Response: ", res);
		matchOutput(res);
		setFlag("ls -l", 1)
		.then(function(res){
			// console.log("Response: ", res);
		})
		.catch(function(err){
			console.log(err)
		});
		return res;
	}).catch(function(){
		setFlag("ls -l", -1)
		.then(function(res){
			console.log(res);
		})
		.catch(function(err){
			console.log(err);
		})
	});
}

function executeCommand(host, port, username, password, cmd){
	var ssh = new node_ssh();
	setFlag(cmd, 0)
	.then(function(res){
		// console.log("Response: ", res);
	})
	.catch(function(err){
		console.log(err)
	});
	return new Promise(function(resolve,reject){
		ssh.connect({
			host: host,
			port: port,
			username: username,
			password: password
		})
		.then(function(){
			ssh.execCommand(cmd).then(function(result) {
				ssh.dispose();
				if(result.stderr) reject(result.stderr);
				if(result.stdout) resolve(result.stdout);
				ssh = null;
			})
		}).catch(function(err){
			reject(err);
		})
	});
}

function setFlag(cmd, value){
	return new Promise(function(resolve,reject){
		RedisClient.hset("abc", "def", "efg")
	})

}

// function matchOutput(res){
// 	console.log("Response: ", res);
// 	DB.troubleshoot_master.findOne({
// 		where: {
// 			slNo: ""
// 		}
// 	})
// }

module.exports = commandExecutor;
