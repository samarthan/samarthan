var node_ssh = require('node-ssh');
var redis = require("./redis_client");

var DB = require('./ecp_db/index');

var RedisCon = new redis();
var RedisClient = RedisCon.redisclient;
const STAGE_CONST = ":stage";
const TXN_COMMAND = ":command";
var array = [];

function jarvis(){};
var method = jarvis.prototype;

method.commandExecution = function (array , object, row){
	console.log("Starting the command execution!.... ");
	return setCommandRunningStatus(object.userId,object.channel,"-1").then(function(){
		return commandExecutor(array,null).then(function(resp_array){
			console.log("Command Execution completed !.... ");
			var status = commandChecker(resp_array) ? 1 : 0;
			return setCommandRunningStatus(object.userId,object.channel,status).then(function(){
				console.log("User hash has been cleared now !.... ");
				//Update status in DB after out valudatrion
				var status = commandChecker(resp_array);
				return commandStatusUpdator(row, JSON.stringify(resp_array), status);
			});
		})
	})
}

method.troubleshootExecution = function(array , object, row){
	console.log("Starting the command execution!.... ");
	return setCommandRunningStatus(object.userId,object.channel,"-1").then(function(){
		return commandExecutor(array, true).then(function(resp_array){
			console.log("Command Execution completed !.... ");
			var status = commandChecker(resp_array) ? 1 : 0;
			return setCommandRunningStatus(object.userId,object.channel,status).then(function(){
				console.log("User hash has been cleared now !.... ");
				//Update status in DB after out valudatrion
				//var status = commandChecker(resp_array);
				return commandStatusUpdator(row, "Troubleshoot Completed!", true);
			});
		})
	})
}

function commandExecutor(array, flag){
	console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	console.dir(array);
	console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	var promises = array.map(function(server){
		var host = server.server.vmIp;
		var port = server.server.sshPort ? server.server.sshPort : 22;
		var username = server.server.userName;
		var password = server.server.keyorPass;
		var commands = flag ? server.troubleshootCommands : server.commands;
		var expected = server.expectedOutput ? server.expectedOutput : null;
		return executeCommand( host, port, username, password, commands, expected );
	})	
	return Promise.all(promises);
}

function commandChecker(responses){
	var flag = true;
	for (var resp in responses){
		console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		var response = responses[resp];
		console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		if(response.stderr) return false;
		else if(response.stdout.toLowerCase() != response.exp.toLowerCase()){
			return false;
		}
	}
	return true;
}

function executeCommand(host, port, username, password, cmd, expected){

	var ssh = new node_ssh();
	return new Promise(function(resolve,reject){
		ssh.connect({
			host: host,
			port: port,
			username: username,
			password: password
		})
		.then(function(){
			console.log("Started Executing");
			ssh.execCommand(cmd).then(function(result) {
				var response = {};
				response.host = host;
				response.stderr = result.stderr;
				response.stdout = result.stdout;
				response.exp = expected;
				ssh = null;
				console.dir(response);
				console.log("Finishing Execution");
				resolve(response);
			})
		}).catch(function(err){
			resolve({ stderr : "CONNECT ERROR"});
		})
	});
}

function setCommandRunningStatus(userid,channel,status){
	console.log("Inside setCommandRunningStatus");
	var channel = channel ? channel : "fb";
	return new Promise(function(resolve,reject){
		var self = this;
		RedisClient.set(userid + ":" + channel + TXN_COMMAND, status ,function(err, reply) {
			console.log("Made the Command status as running " + userid + " is -> " + reply);
			if(err) { console.log("Made the Command status as running " + userid + " has failed -> " + err); reject(err); }
			// reply is null when the key is missing
			if(reply){
				resolve(reply);
			} else {
				//no such command in redis 
				console.log("Couldn't find any history of the User id -> " + userid)
				resolve("-2");
			}
		});
	})
}




function commandStatusUpdator(row, output, status){
	return DB.response_master.find({
		where: {
			slNo: row.slNo
		}
	  }).then(function(record){
		var update = {};
		update.output = output;
		update.execStatus = status ? 1 : 0 ;
		return record.updateAttributes(update);
	});

}

module.exports = jarvis;
